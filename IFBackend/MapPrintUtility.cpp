#include "MapPrintUtility.h"

void MapPrintUtility::print(const Map& m) const {
	auto map = m.get_map();
	for (auto it = map.begin(); it != map.end(); ++it) {
		std::cout << it->first.state->get_id() << " ---- " << it->first.transition << " ----> " << it->second->get_id() << std::endl;
	}
}