#pragma once
#ifndef V_STATEPTRINTER_H
#define V_STATEPTRINTER_H
#include "AbstractStateVisitor.h"
#include "State.h"
#include "Final.h"
#include <string>

/// consumes state ownership and returns the same state without a final decorator (via consume)
class StatePrinter : public AbstractStateVisitor {
private:
	virtual void reset_internal_state() override {};
public:
	virtual void visit(State& x) override {
		std::cout << "NORMAL STATE (id = " << x.get_id() << ")"<< std::endl;
	}
	virtual void visit(Final& x) override {
		std::cout << "FINAL (token = " << x.get_token() << ", id = " << x.get_id() << ")" << std::endl;
		x._component->accept(*this);
	}
};

#endif