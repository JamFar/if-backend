#pragma once
#ifndef V_FINALTONORM_H
#define V_FINALTONORM_H
#include "AbstractStateVisitor.h"
#include "State.h"
#include "Final.h"
#include "v_IsFinal.h"

/// consumes state ownership and returns the same state without a final decorator (via consume)
class v_FinalToNorm : public AbstractStateVisitor{
private:
	v_IsFinal isfin;
	AbstractStateDecorator * parent;
	AbstractState * state;
	virtual void reset_internal_state() override {
		state = nullptr;
		parent = nullptr;
	}
public:
	v_FinalToNorm() {
		state = nullptr;
		parent = nullptr;
	}
	virtual void visit(State& x) override {
		if (state != nullptr) state = &x;
		return;
	}
	virtual void visit(Final& x) override {
		if (state == nullptr) state = x._component.get();
		else parent->_component.reset(x._component.get());
		return;
	}
	std::unique_ptr<AbstractState> consume(std::unique_ptr<AbstractState> _s) {
		AbstractState* s = _s.release();
		s->accept(*this);
		std::unique_ptr<AbstractState> temp(state);
		reset_internal_state();
		temp->accept(isfin);
		if (isfin.is_final()) {
			return consume(std::move(temp));
		}else {
			return temp;
		}
	}
};

#endif