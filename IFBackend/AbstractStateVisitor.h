#pragma once
#ifndef ABSTRACTSTATEVISITOR_H
#define ABSTRACTSTATEVISITOR_H
class State;
class Final;
class AbstractStateVisitor {
protected:
	virtual void reset_internal_state() = 0;
public:
	virtual void visit(State& x) = 0;
	virtual void visit(Final& x) = 0;
};

#endif