#pragma once
#ifndef FINALSTATEMANAGER_H
#define FINALSTATEMANAGER_H

#include "v_FinalToNorm.h"
#include "v_GetToken.h"
#include "v_IsFinal.h"

class FinalStateManager {
private:
	v_FinalToNorm _final_to_norm;
	v_GetToken _get_token;
	v_IsFinal _is_final;
public:
	bool is_final(AbstractState& a){
		a.accept(_is_final);
		return _is_final.is_final();
	}
	std::string get_token(AbstractState& a) {
		a.accept(_get_token);
		return _get_token.get_token();
	}
	std::unique_ptr<AbstractState> to_norm(std::unique_ptr<AbstractState> a) {
		return _final_to_norm.consume(std::move(a));
	}
};

#endif