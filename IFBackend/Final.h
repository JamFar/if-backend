#pragma once
#ifndef FINAL_H
#define FINAL_H
#include "AbstractStateDecorator.h"
#include "_cloneable.h"
#include <iostream>
class Final : public _cloneable<Final, AbstractStateDecorator>{
private:
	std::string token;
	Final* clone_impl() const override;
public:
	Final(std::unique_ptr<AbstractState> component, std::string token);	// decorators are sinks
	std::string get_token() const { return token; }
	int get_id() const override;
	virtual void accept(AbstractStateVisitor&) override;
};


#endif