#pragma once
#ifndef V_ISFINAL_H
#define V_ISFINAL_H
#include "AbstractStateVisitor.h"
#include "State.h"
#include "Final.h"

/// visits a state and returns a bool true if it's final (via isfinal)
class v_IsFinal : public AbstractStateVisitor{
private:
	bool isfinal;
	virtual void reset_internal_state() override {
		isfinal = false;
	}
public:
	v_IsFinal() {
		isfinal = false;
	}
	virtual void visit(State& x) override {
		return;
	}
	virtual void visit(Final& x) override {
		isfinal = true;
		return;
	}
	bool is_final() {
		bool ret = isfinal;
		reset_internal_state();
		return ret;
	}
};

#endif