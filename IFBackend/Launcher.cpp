#include <iostream>
#include "DFA.h"
#include "DFAPrintUtility.h"
#include "MapMemoryUtility.h"
#include "SetMergeUtil.h"
#include "MemoryUtil.h"
#include "DFAManipUtility.h"
#include "Final.h"
#include "StatePrinter.h"
#include "FinalStateManager.h"

int main(void) {

	DFA adfa;

	std::unique_ptr<State> as1 = std::make_unique<State>();
	std::unique_ptr<State> as2 = std::make_unique<State>();
	std::unique_ptr<State> as3 = std::make_unique<State>();

	adfa.set_start_state(as1.get());

	auto _as1 = adfa.add_state(std::move(as1));
	auto _as2 = adfa.add_state(std::move(as2));
	auto _as3 = adfa.add_final_state(std::move(as3), "ab_token");

	std::unique_ptr<Map> amap = std::make_unique<Map>();
	amap->add_entry(_as1, 'a', _as2);
	amap->add_entry(_as2, 'b', _as3);
	adfa.set_map(std::move(amap));

	std::unique_ptr<State> bs1 = std::make_unique<State>();
	std::unique_ptr<State> bs2 = std::make_unique<State>();
	std::unique_ptr<State> bs3 = std::make_unique<State>();

	DFA bdfa;

	bdfa.set_start_state(bs1.get());

	auto _bs1 = bdfa.add_state(std::move(bs1));
	auto _bs2 = bdfa.add_state(std::move(bs2));
	auto _bs3 = bdfa.add_final_state(std::move(bs3), "01_token");

	std::unique_ptr<Map> bmap = std::make_unique<Map>();
	bmap->add_entry(_bs1, '0', _bs2);
	bmap->add_entry(_bs2, '1', _bs3);
	bdfa.set_map(std::move(bmap));

	DFAPrintUtility dpu;
	dpu.print(adfa);
	std::cout << std::endl;
	dpu.print(bdfa);

	DFAManipUtility dmu;
	DFA rdfa;
	dmu.concatenate_dfa(&adfa, &bdfa, &rdfa);

	std::cout << std::endl;
	dpu.print(rdfa);

	std::getchar();
	return 0;
}