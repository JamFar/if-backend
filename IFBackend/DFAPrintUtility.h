#pragma once
#ifndef DFAPRINTUTILITY_H
#define DFAPRINTUTILITY_H
#include "MapPrintUtility.h"
#include "DFA.h"
#include "FinalStateManager.h"
class DFAPrintUtility {
public:
	void print(const DFA& dfa) const {
		MapPrintUtility mpu;
		FinalStateManager fsm;
		std::cout << "Map:" << std::endl;
		mpu.print(*(dfa.get_map()));
		std::cout << std::endl << "Start state: " << dfa.get_start_state()->get_id() << std::endl;
		std::cout << std::endl << "Final state(s): " << std::endl;
		auto fs = dfa.get_final_states();
		for (auto it = fs.begin(); it != fs.end(); ++it) {
			std::cout << "\t" << (*it)->get_id() << "\t" << fsm.get_token(*const_cast<AbstractState*>(*it)) << std::endl;
		}
		std::cout << "------------" << std::endl << std::endl;
	}
};

#endif