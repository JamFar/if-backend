#pragma once
#ifndef MAPMEMORYUTILITY_H
#define MAPMEMORYUTILITY_H
#include "Map.h"
#include <set>
class MapMemoryUtility {
public:
	int remap(Map& a, std::unordered_map<const AbstractState*, const AbstractState*> mapping) const;
	int merge(const Map& a, const Map& b, Map& result) const;
};

#endif