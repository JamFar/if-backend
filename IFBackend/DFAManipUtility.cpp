#include "DFAManipUtility.h"

int DFAManipUtility::merge_map(const DFA* a, const DFA* b, Map& merged_map) const {
	MapMemoryUtility mmu;
	const Map* amap = a->get_map();
	const Map* bmap = b->get_map();
	mmu.merge(*amap, *bmap, merged_map);
	return 1;
}

int DFAManipUtility::merge_states(const DFA* a, const DFA* b, std::set<AbstractState*>& merged_states) const {
	SetMergeUtil smu;
	MemoryUtil memu;
	smu.merge<AbstractState*>(memu.collect<AbstractState>(a->get_states()), memu.collect<AbstractState>(b->get_states()), merged_states);
	return 1;
}

int DFAManipUtility::merge_finals(const DFA* a, const DFA* b, std::set<const AbstractState*>& merged_finals) const {
	SetMergeUtil smu;
	smu.merge<const AbstractState*>(a->get_final_states(), b->get_final_states(), merged_finals);
	return 1;
}

int DFAManipUtility::concatenate_dfa(const DFA* a, const DFA* b, DFA* result) const {
	std::unique_ptr<Map> merged_map = std::make_unique<Map>();
	std::set<const AbstractState*> merged_finals;
	std::set<AbstractState*> merged_states;
	merge_map(a, b, *merged_map);
	merge_states(a, b, merged_states);
	merged_finals = b->get_final_states();

	std::unordered_map<const AbstractState*, const AbstractState*> remap;
	std::set<AbstractState*>::iterator it;
	for (it = merged_states.begin(); it != merged_states.end(); ++it){
		AbstractState* s = *it;
		const AbstractState* news;
		if (merged_finals.find(s) != merged_finals.end()) {
			news = result->add_final_state(s->clone(), "err");
		}else {
			news = result->add_state(s->clone());
		}
		if (s == a->get_start_state()) {
			result->set_start_state(news);
		}
		remap[s] = news;
	}
	MapMemoryUtility mmu;
	mmu.remap(*merged_map, remap);
	result->set_map(std::move(merged_map));
	return 0;
}