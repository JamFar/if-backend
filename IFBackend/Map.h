#pragma once
#ifndef MAP_H
#define MAP_H
#include <iostream>
#include <cstring>
#include <unordered_map>
#include <memory>
#include <string>
#include "AbstractState.h"

// POCO
class StateCharPair {
public:
	const AbstractState* state;	// the pair does not own the state
	char transition;
	StateCharPair(const AbstractState*, char);
	bool operator==(const StateCharPair& stp)const { return (this->state == stp.state && this->transition == stp.transition); }
	void operator=(const StateCharPair& stp) { this->state = stp.state; this->transition = stp.transition; }
};

// overloading hash operator
namespace std { template<> struct hash<StateCharPair> {	size_t operator()(const StateCharPair& s) const; }; }

/// Responsible for creating, deleting, and traversing a hashmap of state - transitions.
class Map {
private:
	std::unordered_map<StateCharPair, const AbstractState*> hashmap;	// the hashmap does not own the states it holds
public:
	Map();
	Map(const Map&);
	std::unordered_map<StateCharPair, const AbstractState*> get_map() const;
	void set_map(std::unordered_map<StateCharPair, const AbstractState*>&);
	std::pair<std::unordered_map<StateCharPair, const AbstractState*>::iterator, bool> add_entry(const AbstractState* s1, const char t, const AbstractState* s2);
	std::unordered_map<StateCharPair, const AbstractState*>::iterator delete_entry(std::unordered_map<StateCharPair, const AbstractState*>::iterator it);
	std::unordered_map<StateCharPair, const AbstractState*>::iterator delete_entry(const AbstractState*, const char t);
	bool exists(const AbstractState* s, const char t);
	std::unordered_map<StateCharPair, const AbstractState*>::iterator find_entry(const AbstractState* s, const char t);
};
#endif