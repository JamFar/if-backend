#pragma once
#ifndef DFAMANIPUTILITY_H
#define DFAMANIPUTILITY_H
#include "DFA.h"
#include "MapMemoryUtility.h"
#include "SetMergeUtil.h"
#include "MemoryUtil.h"
/// Responsible for manipulating DFAs by way of concatenation and union.
class DFAManipUtility {
private:
public:
	int merge_map(const DFA* a, const DFA* b, Map& merged_map) const;
	int merge_states(const DFA* a, const DFA* b, std::set<AbstractState*>& merged_states) const;
	int merge_finals(const DFA* a, const DFA* b, std::set<const AbstractState*>& merged_finals) const;
	int concatenate_dfa(const DFA* a, const DFA* b, DFA* result) const;
	//int optional_concatenate_dfa(const DFA* a, const DFA* b, DFA* result) const;
	//int union_dfa(const DFA* a, const DFA* b, DFA* result) const;
};
#endif