#pragma once
#ifndef STATE_H
#define STATE_H
#include <memory>
#include "AbstractState.h"
#include "_cloneable.h"
/// A POCO for a uniquely-indexed state
class State : public _cloneable<State, AbstractState>{
private:
	int state_id;
	State* clone_impl() const override;
public:
	State();
	int get_id() const override;
	virtual void accept(AbstractStateVisitor&) override;
};
#endif