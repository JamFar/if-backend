#pragma once
#ifndef DFA_H
#define DFA_H
#include "State.h"
#include "Final.h"
#include "Map.h"
#include "MapMemoryUtility.h"
#include "v_IsFinal.h"
#include <set>
#include "v_IsFinal.h"
/// Responsible for storing all the components necessary to form a DFA.
class DFA {
private:
	v_IsFinal v_if;
	std::string input_buffer;
	std::set<std::unique_ptr<AbstractState>> states;	// a DFA owns its states
	std::set<const AbstractState*> final_states;
	const AbstractState* start_state;
	std::unique_ptr<Map> tmap;
	std::string alphabet;
public:
	DFA();
	DFA(const DFA&);
	const std::set<std::unique_ptr<AbstractState>>& get_states() const { return states; };
	const AbstractState* get_start_state() const { return start_state; };
	const std::set<const AbstractState*>& get_final_states() const { return final_states; };
	const Map* get_map() const { return tmap.get(); };
	std::string get_alphabet() const { return alphabet; };
	std::string get_input_buffer() const { return input_buffer; };
	void set_map(std::unique_ptr<Map>);
	void set_start_state(const AbstractState*);
	void set_alphabet(std::string);
	void set_input_buffer(std::string);
	void set_finals(std::set<const AbstractState*> finals);
	const AbstractState* add_state(std::unique_ptr<AbstractState>);	// sink
	const AbstractState* add_final_state(std::unique_ptr<AbstractState>, std::string tokenstr);
	void append_input_buffer(const char);
};
#endif
