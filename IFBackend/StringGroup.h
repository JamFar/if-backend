#pragma once
#ifndef STRINGGROUP_H
#define STRINGGROUP_H

#include <unordered_map>
#include <vector>
#include <memory>

template <typename T>
class StringGroup{
private:
	std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<std::vector<T>>>> group;
public:
	StringGroup();
	StringGroup(const StringGroup<T>&);

	void add_entry(std::string, T);
	std::shared_ptr<std::vector<T>> get_entry(std::string) const;
	std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<std::vector<T>>>> get_hashmap() const;
};

// template implementation

template <typename T>
StringGroup<T>::StringGroup() {
	this->group = std::make_shared<std::unordered_map<std::string, std::shared_ptr<std::vector<T>>>>();
}

template <typename T>
StringGroup<T>::StringGroup(const StringGroup<T>& copyme) {
	this->group = std::make_shared<std::unordered_map<std::string, std::shared_ptr<std::vector<T>>>>();
	for (auto it = copyme.get_hashmap()->begin(); it != copyme.get_hashmap()->end(); ++it) {
		for (int j = 0; j < it->second->size(); j++) {
			add_entry(it->first, it->second->at(j));
		}
	}
}

template <typename T>
void StringGroup<T>::add_entry(std::string type, T object) {
	std::shared_ptr<std::vector<T>> temp;
	auto it = this->group->find(type);
	if (it == this->group->end()) {
		temp = std::make_shared<std::vector<T>>();
		this->group->insert(std::make_pair(type, temp));
	}
	else {
		temp = it->second;
	}
	temp->push_back(object);
}

template <typename T>
std::shared_ptr<std::vector<T>> StringGroup<T>::get_entry(std::string type) const{
	auto it = this->group->find(type);
	if (it == this->group->end()) {
		return nullptr;
	}
	else {
		return it->second;
	}
}

template <typename T>
std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<std::vector<T>>>> StringGroup<T>::get_hashmap() const{
	return this->group;
}

#endif
