#pragma once
#ifndef SETMERGEUTIL_H
#define SETMERGEUTIL_H
#include <set>
#include <memory>
/// Merges any two sets together.
class SetMergeUtil {
public:
	template<class T>
	int merge(const std::set<T>& a, const std::set<T>& b, std::set<T>& result);
};
template<class T>
int SetMergeUtil::merge(const std::set<T>& a, const std::set<T>& b, std::set<T>& result) {
	std::unique_ptr<std::set<T>> _a = std::make_unique<std::set<T>>(a);
	std::unique_ptr<std::set<T>> _b = std::make_unique<std::set<T>>(b);
	result = *_a;
	result.insert(_b->begin(), _b->end());
	return 1;
}
#endif