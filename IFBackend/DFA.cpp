#include "DFA.h"

DFA::DFA(const DFA& copyme) {
	std::set<int> blacklist;
	std::unordered_map<const AbstractState*, const AbstractState*> remap;

	for (auto i = copyme.final_states.begin(); i != copyme.final_states.end(); ++i) {
		blacklist.insert((*i)->get_id());
		std::unique_ptr<AbstractState> cpyfin = (*i)->clone();
		remap[*i] = cpyfin.get();
		final_states.insert(cpyfin.get());
		states.insert(std::move(cpyfin));
	}

	blacklist.insert(copyme.start_state->get_id());
	std::unique_ptr<AbstractState> ss = copyme.start_state->clone();
	remap[copyme.start_state] = ss.get();
	start_state = ss.get();
	states.insert(std::move(ss));	

	for (auto i = copyme.states.begin(); i != copyme.states.end(); ++i) {
		if (blacklist.find((*i)->get_id()) != blacklist.end()) continue;
		std::unique_ptr<AbstractState> cpyfin = (*i)->clone();
		remap[i->get()] = cpyfin.get();
		states.insert(std::move(cpyfin));
	}
	this->tmap = std::make_unique<Map>(*copyme.get_map());
	MapMemoryUtility mmu;
	mmu.remap(*tmap, remap);
}

DFA::DFA() : start_state(nullptr){
	input_buffer = "";
}

void DFA::set_map(std::unique_ptr<Map> m) {
	this->tmap = std::move(m);
}

void DFA::set_finals(std::set<const AbstractState*> finals) {
	this->final_states = finals;
}

const AbstractState* DFA::add_final_state(std::unique_ptr<AbstractState> s, std::string tokenstr) {
	s->accept(v_if);
	std::unique_ptr<AbstractState> news;
	if (!v_if.is_final())
		news = std::make_unique<Final>(std::move(s), tokenstr);
	else
		news = std::move(s);
	auto temp = news.get();
	states.insert(std::move(news));
	final_states.insert(temp);
	return temp;
}

void DFA::set_start_state(const AbstractState* s) {
	this->start_state = s;
}

void DFA::set_alphabet(std::string alphabet) {
	this->alphabet = alphabet;
}

void DFA::set_input_buffer(std::string input_buffer) {
	this->input_buffer = input_buffer;
}

const AbstractState* DFA::add_state(std::unique_ptr<AbstractState> s) {
	auto ret = s.get();	
	states.insert(std::move(s));
	return ret;
}

void DFA::append_input_buffer(char c) {
	this->input_buffer += c;
}