#pragma once
#ifndef ABSTRACTSTATE_H
#define ABSTRACTSTATE_H
#include "_cloneable.h"
#include <memory>
#include "AbstractStateVisitor.h"
class AbstractState: public _cloneable<AbstractState>{
protected:
	static int class_id;
public:
	virtual int get_id() const = 0;
	virtual void accept(AbstractStateVisitor& v) = 0;
};
#endif