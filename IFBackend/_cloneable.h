#pragma once
#ifndef _CLONEABLE_H
#define _CLONEABLE_H
#include <memory>

// specialisation for concrete nodes, supporting multiple inheritance
template <typename Derived, typename ... Bases>
class _cloneable : public Bases...{
protected:
	virtual _cloneable* clone_impl() const = 0;
public:
	std::unique_ptr<Derived> clone() const{
		return std::unique_ptr<Derived>(static_cast<Derived *>(this->clone_impl()));
	}
};

// specialisation for nodes that do not need a base class to inherit clone_impl from
template <typename Derived>
class _cloneable<Derived>{
protected:
	virtual _cloneable * clone_impl() const = 0;
public:
	std::unique_ptr<Derived> clone() const {
		return std::unique_ptr<Derived>(static_cast<Derived *>(this->clone_impl()));
	}
};
#endif