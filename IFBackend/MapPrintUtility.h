#pragma once
#ifndef MAPPRINTUTILITY_H
#define MAPPRINTUTILITY_H
#include "Map.h"
class MapPrintUtility {
public:
	void print(const Map&) const;
};

#endif