#pragma once
#ifndef ABSTRACTSTATEDECORATOR_H
#define ABSTRACTSTATEDECORATOR_H
#include "AbstractState.h"
#include "_cloneable.h"
#include <memory>
class AbstractStateDecorator : public _cloneable<AbstractStateDecorator, AbstractState>{
public:
	std::unique_ptr<AbstractState> _component;
};
#endif