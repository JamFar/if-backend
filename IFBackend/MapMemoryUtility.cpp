#include "MapMemoryUtility.h"

int MapMemoryUtility::merge(const Map& a, const Map& b, Map& result) const {
	std::unique_ptr<Map> _a = std::make_unique<Map>(a);
	std::unique_ptr<Map> _b = std::make_unique<Map>(b);
	auto result_map = _a->get_map();
	auto bmap = _b->get_map();
	result_map.insert(bmap.begin(), bmap.end());
	result.set_map(result_map);
	return 1;
}

int MapMemoryUtility::remap(Map& a, std::unordered_map<const AbstractState*, const AbstractState*> mapping) const {
	std::unordered_map<StateCharPair, const AbstractState*> map = a.get_map();
	std::unordered_map<StateCharPair, const AbstractState*> newmap;
	for (std::unordered_map<StateCharPair, const AbstractState*>::iterator it = map.begin(); it != map.end(); ++it) {
		const AbstractState* fr = it->first.state;
		const AbstractState* to = it->second;
		
		auto temp = mapping.find(fr);
		const AbstractState* newfr;
		if (temp != mapping.end())
			newfr = mapping.find(fr)->second;
		else
			newfr = fr;

		temp = mapping.find(to);
		const AbstractState* newto;
		if (temp != mapping.end())
			newto = mapping.find(to)->second;
		else
			newto = to;

		char trans = it->first.transition;
		newmap.insert(std::pair<StateCharPair, const AbstractState*>(StateCharPair(newfr, trans), newto));
	}
	a.set_map(newmap);
	return 1;
}