#pragma once
#ifndef MEMORYUTIL_H
#define MEMORYUTIL_H
#include <vector>
#include <set>
#include <memory>
/// Used to convert containers of owned objects to containers of objects with no defined ownership (unique_ptr to raw).
class MemoryUtil {
public:
	template <class T>
	std::vector<T*> collect(const std::vector<std::unique_ptr<T>>&) const;
	template <class T>
	std::set<T*> collect(const std::set<std::unique_ptr<T>>&) const;
};
template <class T>
std::vector<T*> MemoryUtil::collect(const std::vector<std::unique_ptr<T>>& data) const {
	std::vector<T*> result;
	for (auto const & it : data) { result.push_back(it.get()); }
	return result;
}
template <class T>
std::set<T*> MemoryUtil::collect(const std::set<std::unique_ptr<T>>& data) const {
	std::set<T*> result;
	for (auto const & it : data) { result.insert(it.get()); }
	return result;
}
#endif