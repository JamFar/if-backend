#include "Map.h"

StateCharPair::StateCharPair(const AbstractState* s, char c) : transition(c) {
	this->state = s;
}

std::size_t std::hash<StateCharPair>::operator()(const StateCharPair& s) const {
	return (hash<std::string>()(std::to_string(s.state->get_id()) + s.transition));
}

Map::Map() {
	this->hashmap = std::unordered_map<StateCharPair, const AbstractState*>();
}

Map::Map(const Map& copyme) {
	this->hashmap = copyme.get_map();
}

std::unordered_map<StateCharPair, const AbstractState*> Map::get_map() const{
	return this->hashmap;
}

void Map::set_map(std::unordered_map<StateCharPair, const AbstractState*>& hashmap) {
	this->hashmap = hashmap;
}

std::pair<std::unordered_map<StateCharPair, const AbstractState*>::iterator, bool> Map::add_entry(const AbstractState* s1, const char t, const AbstractState* s2){
	StateCharPair stp(s1, t);
	return this->hashmap.insert(std::pair<StateCharPair, const AbstractState*>(stp, s2));
}

std::unordered_map<StateCharPair, const AbstractState*>::iterator Map::find_entry(const AbstractState* s, const char t) {
	StateCharPair stp(s, t);
	return hashmap.find(stp);
}

bool Map::exists(const AbstractState* s, const char t) {
	if (hashmap.find(StateCharPair(s, t)) == hashmap.end()) return false;
	return true;
}

std::unordered_map<StateCharPair, const AbstractState*>::iterator Map::delete_entry(std::unordered_map<StateCharPair, const AbstractState*>::iterator it){
	return this->hashmap.erase(it);
}

std::unordered_map<StateCharPair, const AbstractState*>::iterator Map::delete_entry(const AbstractState* s, const char t){
	return delete_entry(find_entry(s, t));
}