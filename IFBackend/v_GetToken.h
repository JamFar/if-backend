#pragma once
#ifndef V_GETTOKEN_H
#define V_GETTOKEN_H
#include "AbstractStateVisitor.h"
#include "State.h"
#include "Final.h"

/// visits a state and returns a bool true if it's final (via isfinal)
class v_GetToken : public AbstractStateVisitor {
private:
	std::string token;
	virtual void reset_internal_state() override {
		token = "";
	}
public:
	v_GetToken() {
		token = "";
	}
	virtual void visit(State& x) override {
		return;
	}
	virtual void visit(Final& x) override {
		token = x.get_token();
		return;
	}
	std::string get_token() {
		std::string ret = token;
		reset_internal_state();
		return ret;
	}
};

#endif