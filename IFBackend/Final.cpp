#include "Final.h"

Final* Final::clone_impl() const {
	std::unique_ptr<AbstractState> ret = this->_component->clone();
	return new Final(std::move(ret), this->token);
}

Final::Final(std::unique_ptr<AbstractState> component, std::string token) {
	this->_component = std::move(component);
	this->token = token;
}

int Final::get_id() const {
	return this->_component->get_id();
}

void Final::accept(AbstractStateVisitor& v) {
	v.visit(*this);
}