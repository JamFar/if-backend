#include "State.h"

State* State::clone_impl() const {
	return new State();
}

State::State() {
	this->state_id = class_id;
	AbstractState::class_id++;
}

int State::get_id() const{
	return this->state_id;
}

void State::accept(AbstractStateVisitor& v) {
	v.visit(*this);
}